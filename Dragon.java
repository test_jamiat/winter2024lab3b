public class Dragon{
	public boolean wings;
	public boolean noHorns;
	public boolean spitsFire;
	
	public String blowsFire;
	public String hasNoHorns;
	public String canFly;
	
	public Dragon(boolean canFly, boolean hasNoHorns, boolean spitsFire){
		wings = canFly;
		noHorns = hasNoHorns;
		this.spitsFire = spitsFire;
	}
	
	public String dragonFire(){
		if(spitsFire == true){
			blowsFire = "🌬 I know that's right";
		} else{
			blowsFire = "Side eye...";
		}
		return blowsFire;
	}
	
	public String hasWings(){
		if(wings == true){
			canFly = "Alexa, play I'm levitating by Dua Lipa";
		} else{
			canFly = "Just because you can't fly doesn't mean I also can't";
		}
		return canFly;
	}
	
	public String dragonHornSupremacy(){
		if(noHorns == true){
			hasNoHorns = "In horns we believe 😈😈";
		} else{
			hasNoHorns = "Do I look like I wouldn't have horns?";
		}
		return hasNoHorns;
	}
}