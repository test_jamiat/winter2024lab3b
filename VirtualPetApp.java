import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Dragon[] mythicalCreatures = new Dragon[4];
		System.out.println("Please input true or false for each statement");
		
		for(int i = 0; i < mythicalCreatures.length; i++){
			Scanner reader = new Scanner(System.in);
				
			System.out.println("It can spit fire");
			boolean spitsFire = reader.nextBoolean();
			
			System.out.println("It can fly");
			boolean canFly = reader.nextBoolean();
		
			System.out.println("Does not have horns");
			boolean hasNoHorns = reader.nextBoolean();

			mythicalCreatures[i] = new Dragon(canFly, hasNoHorns, spitsFire);
		}
		
		System.out.println(mythicalCreatures[0].dragonFire());
		System.out.println(mythicalCreatures[0].hasWings());
		System.out.println(mythicalCreatures[0].dragonHornSupremacy());
	}
}